package com.example.mohamzsiad.imagegallery

/**
 * Created by moham on 27-07-2019.
 */
data class GalleryModel(
        val photos: Photos?,
        val stat: String?
)

data class Photos(
        val page: Int?,
        val pages: Int?,
        val perpage: Int?,
        val total: Int?,
        val photo: List<PhotoModel>?
)

data class PhotoModel(
        val id: String?,
        val owner: String?,
        val secret: String?,
        val server: String?,
        val farm: Int?,
        val title: String?,
        val ispublic: Int?,
        val isfriend: Int?,
        val isfamily: Int?,
        val url_s: String?,
        val height_s: String?,
        val width_s: String?
)
