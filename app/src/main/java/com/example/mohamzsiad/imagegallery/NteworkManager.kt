package com.example.mohamzsiad.imagegallery

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest

class NetworkStateManager : ConnectivityManager.NetworkCallback() {

    private val networkRequest: NetworkRequest = NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR).addTransportType(NetworkCapabilities.TRANSPORT_WIFI).build()

    lateinit var connectivityManager: ConnectivityManager
    fun enable(context: Context) {
        connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.registerNetworkCallback(networkRequest, this)
    }

    override fun onAvailable(network: Network) {
        isNetworkAvailable = true
    }

    override fun onLost(network: Network?) {
        isNetworkAvailable = false
    }

    override fun onUnavailable() {
        isNetworkAvailable = false
    }

    companion object {
        var isNetworkAvailable = false
    }
}