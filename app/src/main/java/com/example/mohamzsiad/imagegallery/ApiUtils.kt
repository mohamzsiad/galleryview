package com.example.mohamzsiad.imagegallery


class Api{
    companion object ApiUtils {
        const val BASE_URL = "https://api.flickr.com/"

        val apiService: ApiManager
            get() = RetrofitClientInstance.getClient(BASE_URL)!!.create(ApiManager::class.java)


        fun resetRetrofitClient() {
            RetrofitClientInstance.resetClient()
        }
    }
}