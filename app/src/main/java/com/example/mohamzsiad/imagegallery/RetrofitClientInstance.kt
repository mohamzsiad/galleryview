package com.example.mohamzsiad.imagegallery

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.concurrent.TimeUnit

class RetrofitClientInstance {
    companion object {
        private var retrofit: Retrofit? = null
        fun getClient(baseUrl: String): Retrofit? {
            if (retrofit == null) {
                val okHttpClient = OkHttpClient().newBuilder()
                    var interceptor = Interceptor { chain ->
                        val request = chain.request().newBuilder().build()
                        chain.proceed(request)
                    }
                    okHttpClient.networkInterceptors().add(interceptor)
                retrofit = Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttpClient.build())
                        .build()
            }
            return retrofit
        }

        fun resetClient(){
            retrofit = null
        }
    }
}