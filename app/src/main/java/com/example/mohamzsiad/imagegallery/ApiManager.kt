package com.example.mohamzsiad.imagegallery

import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by moham on 27-07-2019.
 */
interface ApiManager {

    @GET("services/rest/?method=flickr.photos.getRecent&per_page=20&page=1&api_key=6f102c62f41998d151e5a1b48713cf13&format=json&nojsoncallback=1&extras=url_s")
    fun getImages(): Observable<GalleryModel>

    @GET("services/rest/?method=flickr.photos.getRecent&per_page=20&page=1&api_key=6f102c62f41998d151e5a1b48713cf13&format=json&nojsoncallback=1&extras=url_s")
    fun imagePaging(@Query("page") page: Int): Observable<GalleryModel>

    @GET("services/rest/?method=flickr.photos.search&api_key=6f102c62f41998d151e5a1b48713cf13&format=json&nojsoncallback=1&extras=url_s&text=cat")
    fun imageSearch(@Query("text") text: String): Observable<GalleryModel>

}