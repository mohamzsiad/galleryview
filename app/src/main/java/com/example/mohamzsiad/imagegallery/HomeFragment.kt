package com.example.mohamzsiad.imagegallery

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : Fragment() {

    private var galleryResponse: MutableLiveData<GalleryModel> = MutableLiveData()
    private var commonError: MutableLiveData<String> = MutableLiveData()
    private lateinit var homeView: View
    private lateinit var homeContext: Context
    var galleryModel: GalleryModel? = null
    var photoList = ArrayList<PhotoModel>()
    private lateinit var imageAdapter: ImageAdapter
    var totalPages = 0
    var currentPage = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity?.let {
            homeContext = it
        }
        homeView = inflater.inflate(R.layout.fragment_home, container,
                false)

        initiate()
        homeView.btn_loadMore.setOnClickListener {
            imagepaging(currentPage)
        }
        return homeView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageAdapter = ImageAdapter(photoList, homeContext)
        recyclerViewImage.layoutManager =  GridLayoutManager (homeContext,3, GridLayoutManager.VERTICAL, false)
        recyclerViewImage.adapter = imageAdapter
        imageAdapter.notifyDataSetChanged()
        recyclerViewImage.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (!recyclerView.canScrollVertically(1) && dy != 0) {
                    if (totalPages != currentPage) {
                        currentPage = (currentPage + 1)
                        btn_loadMore.isEnabled =  true
                    }
                }
            }
        })
    }

    private fun initiate() {
        if (NetworkStateManager.isNetworkAvailable) {
            Api.apiService.getImages().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::getImagesResponse, this::handleError)
        } else {
            Snackbar.make(homeView,"Internet not available. Try again",Snackbar.LENGTH_LONG).setAction("retry", {
                initiate()
            }).show()
            homeView.btn_loadMore.isEnabled = true
        }
    }

    fun imagepaging(page:Int) {
        if (NetworkStateManager.isNetworkAvailable) {
            Api.apiService.imagePaging(page).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::getImagesResponse, this::handleError)
        } else {
            Snackbar.make(homeView,"Internet not available. Try again",Snackbar.LENGTH_LONG).setAction("retry", {
                imagepaging(currentPage)
            }).show()
        }
    }

    private fun getImagesResponse(apiResponse: GalleryModel) {
        galleryResponse.value = apiResponse
        apiResponse.let {
            galleryModel = it
            totalPages = it.photos?.pages!!
        }
        btn_loadMore.isEnabled =  false
        setAdapter()
    }

    private fun handleError(error:Throwable) {
        commonError.value = error.message
    }

    private fun setAdapter() {
        galleryModel?.photos?.photo?.let { photoList.addAll(it) }
        imageAdapter.notifyDataSetChanged()
    }
}
