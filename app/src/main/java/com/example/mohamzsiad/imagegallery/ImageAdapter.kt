package com.example.mohamzsiad.imagegallery

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.imageitem.view.*


class ImageAdapter(private val imageList: ArrayList<PhotoModel>, val context: Context) : RecyclerView.Adapter<ImageAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.imageitem, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setImage(imageList[position],context)
    }

    override fun getItemCount(): Int {
       return imageList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun setImage(image: PhotoModel, context: Context) {
            Glide.with(context)
                    .load(image.url_s)
                    .placeholder(android.R.drawable.ic_menu_gallery)
                    .into(itemView.item_imageView)
        }
    }

}