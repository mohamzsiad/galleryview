package com.example.mohamzsiad.imagegallery

import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var networkStateReceiver: NetworkStateManager? = NetworkStateManager()
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                loadFragment(supportFragmentManager,HomeFragment(),R.id.mainLayout,false)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                loadFragment(supportFragmentManager, SearchFragment(),R.id.mainLayout,false)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadFragment(supportFragmentManager,HomeFragment(),R.id.mainLayout,false)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        networkStateReceiver?.enable(this)
    }

    fun loadFragment(fragmentManager: FragmentManager?, navFragment: Fragment, containerID: Int, backEnabled: Boolean = true) {
        val transaction =  fragmentManager?.beginTransaction()
        transaction?.replace(containerID, navFragment, navFragment.javaClass.simpleName)
        transaction?.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
        if (backEnabled) {
            transaction?.addToBackStack(navFragment.javaClass.simpleName)
        }
        transaction?.commit()
    }
}
