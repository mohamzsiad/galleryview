package com.example.mohamzsiad.imagegallery

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_search.*


class SearchFragment : Fragment() {

    private var searchResponse: MutableLiveData<GalleryModel> = MutableLiveData()
    private var commonError: MutableLiveData<String> = MutableLiveData()
    private lateinit var searchView: View
    private lateinit var searchContext: Context
    var galleryModel: GalleryModel? = null
    var photoList = ArrayList<PhotoModel>()
    private lateinit var imageAdapter: ImageAdapter
    var searchText = "a"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity?.let {
            searchContext = it
        }
        searchView = inflater.inflate(R.layout.fragment_search, container,
                false)

        initiate(searchText)
        return searchView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageAdapter = ImageAdapter(photoList, searchContext)
        recyclerSearchImage.layoutManager =  GridLayoutManager (searchContext,3, GridLayoutManager.VERTICAL, false)
        recyclerSearchImage.adapter = imageAdapter
        imageAdapter.notifyDataSetChanged()
        btnSearch.setOnClickListener {
            searchFunction()
        }
    }

    fun searchFunction() {
        if (editTextSearch.text.isEmpty()) {
            initiate(searchText)
        } else {
            initiate(editTextSearch.text.toString())
        }
    }

    private fun initiate(searchKey:String) {
        if (NetworkStateManager.isNetworkAvailable) {
            Api.apiService.imageSearch(searchKey).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::getImagesResponse, this::handleError)
        } else {
            Snackbar.make(searchView,"Internet not available. Try again", Snackbar.LENGTH_LONG).setAction("retry", {
               searchFunction()
            }).show()
        }
    }

    private fun getImagesResponse(apiResponse: GalleryModel) {
        searchResponse.value = apiResponse
        apiResponse.let {
            galleryModel = it
        }
        setAdapter()
    }

    private fun handleError(error:Throwable) {
        commonError.value = error.message
    }

    private fun setAdapter() {
        photoList.clear()
        galleryModel?.photos?.photo?.let { photoList.addAll(it) }
        imageAdapter.notifyDataSetChanged()
    }
}

